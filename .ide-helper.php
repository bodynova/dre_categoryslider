<?php
namespace Bender\dre_CategorySlider\Application\Controller\Admin;
class ActionsCategoryAjax extends \OxidEsales\Eshop\Application\Controller\Admin\ListComponentAjax{}
class dre_actionsmain extends \OxidEsales\Eshop\Application\Controller\Admin\ActionsMain{}

namespace Bender\dre_CategorySlider\Core;
class dre_ViewConfig extends \OxidEsales\Eshop\Core\ViewConfig{}

namespace Bender\dre_CategorySlider\Application\Model;
class dre_Actions extends \OxidEsales\Eshop\Application\Model\Actions{}
class dre_ActionList extends \OxidEsales\Eshop\Application\Model\ActionList{}
