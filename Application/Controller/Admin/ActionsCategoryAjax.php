<?php
namespace Bender\dre_CategorySlider\Application\Controller\Admin;

use \OxidEsales\Eshop\Application\Controller\Admin;
use \OxidEsales\Eshop\Core\Registry;
use \OxidEsales\Eshop\Core;
use \OxidEsales\Eshop\Core\Field;
use oxRegistry;
use oxDb;
use oxField;

/**
 * Class controls action assignment to category
 *
 * \OxidEsales\Eshop\Application\Controller\Admin\ListComponentAjax
 *
 */
class ActionsCategoryAjax extends \OxidEsales\Eshop\Application\Controller\Admin\ListComponentAjax {

    /**
     * Columns array
     *
     * @var array
     */
    protected $_aColumns = [
        //field, table, visible, multilanguage, ident
        'container1' => [
            ['oxtitle', 'oxcategories', 1, 1, 0],
            ['oxdesc',  'oxcategories', 1, 1, 0],
            ['oxid',    'oxcategories', 1, 0, 0],
            ['oxactive','oxcategories', 1, 1, 0],
            ['oxid',    'oxcategories', 1, 0, 1]
        ]
    ];

    /**
     * Returns SQL query for data to fetc
     *
     * @return string
     */
    protected function _getQuery()
    {
        $oCat = oxNew(\OxidEsales\Eshop\Application\Model\Category::class);
        $oCat->setLanguage( \OxidEsales\Eshop\Core\Registry::getConfig()->getRequestParameter('editlanguage') );

        $sCategoriesTable = $oCat->getViewName();

        return " from $sCategoriesTable where " . $oCat->getSqlActiveSnippet();
    }

    /**
     * Returns SQL query for data to fetc
     *
     * @return string
     */
    /*
    protected function _getQuery()
    {
        // active AJAX component
        $sGroupTable = $this->_getViewName('oxgroups');
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();

        $sId = \OxidEsales\Eshop\Core\Registry::getConfig()->getRequestParameter('oxid');
        $sSynchId = \OxidEsales\Eshop\Core\Registry::getConfig()->getRequestParameter('synchoxid');

        // category selected or not ?
        if (!$sId) {
            $sQAdd = " from {$sGroupTable} where 1 ";
        } else {
            $sQAdd = " from oxobject2action, {$sGroupTable} where {$sGroupTable}.oxid=oxobject2action.oxobjectid " .
                " and oxobject2action.oxactionid = " . $oDb->quote($sId) .
                " and oxobject2action.oxclass = 'oxgroups' ";
        }

        if ($sSynchId && $sSynchId != $sId) {
            $sQAdd .= " and {$sGroupTable}.oxid not in ( select {$sGroupTable}.oxid " .
                "from oxobject2action, {$sGroupTable} where $sGroupTable.oxid=oxobject2action.oxobjectid " .
                " and oxobject2action.oxactionid = " . $oDb->quote($sSynchId) .
                " and oxobject2action.oxclass = 'oxgroups' ) ";
        }

        return $sQAdd;
    }
    */


    /**
     * Unassign category
     */
    public function unassignCat()
    {
        $sActionId = \OxidEsales\Eshop\Core\Registry::getConfig()->getRequestParameter('oxid');
        if ($sActionId && $sActionId != "-1") {
            $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();
            $sQ = "delete from dreaction2category where oxactionid=" . $oDb->quote($sActionId);
            $oDb->Execute($sQ);
        }
    }




    /**
     * Assign category to action
     */
    public function assignCat()
    {
        // first delete eventually former assigned category
        $this->unassignCat();
        
        //new assignment
        $sChosenCat = \OxidEsales\Eshop\Core\Registry::getConfig()->getRequestParameter('oxcatid');
        $sActionId = \OxidEsales\Eshop\Core\Registry::getConfig()->getRequestParameter('oxid');
        if ($sActionId && $sActionId != "-1" && $sChosenCat && $sChosenCat != "-1") {
            $oNew = oxNew(\OxidEsales\Eshop\Core\Model\BaseModel::class);
            $oNew->init('dreaction2category');
            $oNew->dreaction2category__oxactionid = new Field($sActionId);
            $oNew->dreaction2category__oxcatid = new Field($sChosenCat);
            $oNew->save();
        }
    }

    
    
}
class_alias(\Bender\dre_CategorySlider\Application\Controller\Admin\ActionsCategoryAjax::class, 'actionscategory_ajax');
