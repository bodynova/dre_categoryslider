<?php
namespace Bender\dre_CategorySlider\Application\Controller\Admin;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Request;
//use OxidEsales\Eshop\Application\Controller\Admin\ActionsMain;
use Bender\dre_CategorySlider\Application\Controller\Admin\ActionsCategory;
//use Bender\dre_CategorySlider\Application\views\admin\tpl;
/**
 * Purpose:     Extension of actions_main.php.
 *              Overwrites the render method
 *              of the parent class to use own template.
 *              Admin Menu: Manage Products -> actions -> Main.
 * 
 */
class dre_actionsmain extends \OxidEsales\Eshop\Application\Controller\Admin\ActionsMain {

    public function render() {

        $return = parent::render();

        // product assignment with ajax popup
        if (($oPromotion = $this->getViewDataElement("edit"))) {
            if (($oPromotion->oxactions__oxtype->value == 9)) {
                if ($iAoc = Registry::get(Request::class)->getRequestEscapedParameter("oxpromotionaoc")) {
                    $sPopup = false;
                    switch ($iAoc) {
                        case 'article':
                            // generating category tree for select list
                            $this->_createCategoryTree("artcattree", $soxId);

                            if ($oArticle = $oPromotion->getBannerArticle()) {
                                $this->_aViewData['actionarticle_artnum'] = $oArticle->oxarticles__oxartnum->value;
                                $this->_aViewData['actionarticle_title'] = $oArticle->oxarticles__oxtitle->value;
                            }

                            $sPopup = 'actions_article';
                            $sTemplate = "popups/{$sPopup}.tpl";
                            break;
                        case 'groups':
                            $sPopup = 'actions_groups';
                            $sTemplate = "popups/{$sPopup}.tpl";
                            break;
                        case 'category':
                            $sPopup = 'actionscategory';
                            //$sTemplate = Registry::getConfig()->getModulesDir()."bender/dre_categoryslider/Application/views/admin/tpl/popups/{$sPopup}.tpl";
                            $sTemplate = "{$sPopup}.tpl";
                            break;
                    }

                    if ($sPopup) {
                        $aColumns = array();
                        $oActionsArticleAjax = oxNew(\Bender\dre_CategorySlider\Application\Controller\Admin\ActionsCategoryAjax::class);


                        $this->_aViewData['oxajax'] = $oActionsArticleAjax->getColumns();
                        
                        $sAssignedCatId = $this->_getAssignedCatId($oPromotion->oxactions__oxid->value);
                        if ($sAssignedCatId) {
                            $oCat = oxNew("oxCategory");
                            if ($oCat->load($sAssignedCatId)) {
                                $this->_aViewData["defcat"] = $oCat;
                            }
                        }
                        /*
                        echo '<pre>';
                        print_r($this);
                        die();
                        */
                        //die($return);
                        //return 'ActionsCategory.tpl';
                        return $sTemplate; //dirname( __DIR__ , 2).'/views/admin/tpl/popups/ActionsCategory.tpl';//$sTemplate;
                    }
                }
            }
        }
        
        if($return == 'actions_main.tpl') {
            /*
            echo '<pre>';
            print_r($sTemplate);
            die();
            */
            return 'dre_actions_main.tpl';
        } else {
            /*
            echo '<pre>';
            print_r($sTemplate);
            die();
            */
            return $return;
        }
    }
    
    protected function _getAssignedCatId($sActionId){
        $sCatId = false;
        if ($sActionId && $sActionId != "-1") {
            $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();
            $sActionId = $oDb->quote($sActionId);

            $sSelect = "select oxcatid from dreaction2category where oxactionid=$sActionId";
            $sCatId = $oDb->getOne($sSelect);
        }
        return $sCatId;
    }

}
