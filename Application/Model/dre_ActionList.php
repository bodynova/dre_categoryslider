<?php
namespace Bender\dre_CategorySlider\Application\Model;


class dre_ActionList extends dre_ActionList_parent
{
    /**
     * return true if there are any active slides of category
     *
     * @param String $sCatId
     * 
     * @return boolean
     */
    public function areAnyActiveSlides($sCatId)
    {
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();
        $sCatId = $oDb->quote($sCatId);
        $sQ = "select 1 from " . getViewName('oxactions') . " left join dreaction2category on " .
              getViewName('oxactions') . ".oxid=dreaction2category.oxactionid " .
              "where oxtype=9 and oxactive=1 and dreaction2category.oxcatid=$sCatId limit 1";
        
        return (bool) $oDb->getOne($sQ);
    }


    /**
     * load active shop banner list
     */
    public function loadSlides($sCatId)
    {
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();
        $sCatId = $oDb->quote($sCatId);
        $oBaseObject = $this->getBaseObject();
        $oViewName = $oBaseObject->getViewName();
        $sQ = "select {$oViewName}.* from {$oViewName} left join dreaction2category on "
              . "{$oViewName}.oxid=dreaction2category.oxactionid "
              . "where oxtype=9 and " . $oBaseObject->getSqlActiveSnippet()
              . " and dreaction2category.oxcatid=$sCatId "
              . $this->_getUserGroupFilter() . " order by oxsort";
        $this->selectString($sQ);
    }

}
