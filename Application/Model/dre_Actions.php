<?php
namespace Bender\dre_CategorySlider\Application\Model;

use OxidEsales\Eshop\Core\Registry;

/**
 * Purpose:     Extension of oxactions.php.
 *              Extends the model to handle dre_category_slider
 * 
 */
class dre_Actions extends dre_Actions_parent{
     /**
     * return assigned slide category
     *
     * @return oxArticle
     */
    public function getSlideCategory()
    {
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();
        $sCatId = $oDb->getOne(
            'select oxcatid from dreaction2category '
            . 'where oxactionid=' . $oDb->quote($this->getId())
        );

        if ($sCatId) {
            $oCategory = oxNew( \OxidEsales\Eshop\Application\Model\Category::class );

            if ($this->isAdmin()) {
                $oCategory->setLanguage( Registry::getLang()->getEditLanguage() );
            }

            if ($oCategory->load($sCatId)) {
                return $oCategory;
            }
        }

        return null;
    }

    public function getSlideType()
    {
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();

        return "Slider Type TEST";

    }


}
