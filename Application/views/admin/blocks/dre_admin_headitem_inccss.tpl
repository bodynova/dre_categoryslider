[{$smarty.block.parent}]

[{*
<link rel="stylesheet" href="[{$oViewConf->getModuleUrl('dre_categoryslider','out/src/css/dre_admin_bootstrap.css')}]">
 *}]

[{if $oView->getClassName() == "actions_main"}]

<!-- Das neueste kompilierte und minimierte CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- Optionales Theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Hind:300,400,500,600|Kodchasan:300,400,500,600|Pacifico|Raleway:300,400,500,600" rel="stylesheet">

<!-- chosen -->
[{*}]
<link rel="stylesheet" href="[{$oViewConf->getModuleUrl('dre_categoryslider','out/src/css/chosen.css')}]">
[{*}]
[{* select2 *}]
[{*}]
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
[{*}]

[{* Bootstrapo Multiselect *}]
<link rel="stylesheet" type="text/css" href="[{$oViewConf->getModuleUrl('dre_categoryslider','out/src/css/bootstrap-multiselect.css')}]"/>

[{* Color Picker Plugin *}]
<link rel="stylesheet" type="text/css" href="[{$oViewConf->getModuleUrl('dre_categoryslider','out/src/css/spectrum.css')}]"/>

<style>

/* text classen */

	/* color */
	.font-color-bn-petrol{
		color: #60a3b4;
	}
	.font-color-bn-hellpetrol{
		color: #709CD8;
	}
	.font-color-bn-rot{
		color: #bd1622;
	}
	.font-color-bn-grau{
		color: #626261;
	}
	.font-color-bn-header-grau{
		color: #F2F2F2;
	}
	.font-color-bn-lila{
		color: #776AB4;
	}

	/* font-weight */
	.font-weight-light{
		font-weight: 300;
	}
	.font-weight-medium{
		font-weight: 400;
	}
	.font-weight-semibold{
		font-weight: 500;
	}
	.font-weight-bold{
		font-weight: 600;
	}

	/* font-family */
	.font-hind{
		font-family: 'Hind', sans-serif;
	}
	.font-kodachasan{
		font-family: 'Kodchasan', sans-serif;
	}
	.font-raleway{
		font-family: 'Raleway', sans-serif;
	}
	.font-pacifico{
		font-family: 'Pacifico', cursive;
	}

	/* font-size */
	.font-size-16{
		font-size: 16px;
	}
	.font-size-18{
		font-size:18px;
	}
	.font-size-20{
		font-size:20px;
	}
	.font-size-22{
		font-size: 22px;
	}
	.font-size-24 {
		font-size: 24px;
	}
	.font-size-24 {
		font-size: 24px;
	}
	.font-size-26 {
		font-size: 26px;
	}
	.font-size-28 {
		font-size: 28px;
	}
	.font-size-30 {
		font-size: 30px;
	}
	.font-size-32 {
		font-size: 32px;
	}
	.font-size-34 {
		font-size: 34px;
	}
	.font-size-36 {
		font-size: 36px;
	}
	.font-size-38 {
		font-size: 38px;
	}
	.font-size-40 {
		font-size: 40px;
	}
	.font-size-42 {
		font-size: 42px;
	}
	.font-size-44 {
		font-size: 44px;
	}
	.font-size-46 {
		font-size: 46px;
	}
	.font-size-48 {
		font-size: 48px;
	}
	.font-size-50 {
		font-size: 50px;
	}
	/* text-align */
	.text-center{
		text-align: center;
	}
	.text-left{
		text-align: left;
	}
	.text-right{
		text-align: right;
	}
	.text-justify{
		text-align: justify;
	}

/* Box Klassen */

	/* background */
	.background-white{
		background-color: white;
	}
	.background-opacity-2{
		opacity: 0.2;
	}
	.border-radius-5{
		border-radius: 5px;
	}
	.border-radius-10{
		border-radius: 10px;
	}
	.border-radius-25{
		border-radius: 25px;
	}
	.border-radius-50{
		border-radius: 50px;
	}


/*  */

	.height-25 {
		height: 50px;
	}
	.height-50{
		height:100px;
	}

	.height-100{
		height: 100px;
	}
	.height-200{
		height:200px;
	}
	.height-300{
		height:300px;
	}

	/* -------- */

	#feedback {
		font-size: 1.4em;
	}

	#selectable .ui-selecting {
		background: #FECA40;
	}

	#selectable .ui-selected {
		background: #F39814;
		color: white;
	}

	#selectable {
		list-style-type: none;
		margin: 0;
		padding: 0;
		width: 250px;
	}

	#selectable li {
		margin: 3px;
		padding: 1px;
		float: left;
		width: 50px;
		height: 40px;
		font-size: 2em;
		text-align: center;
	}

.awesome {
	background: #60a3b4;
}

/* font-weight */
.font-weight-light {
	font-weight: 300;
}

.font-weight-medium {
	font-weight: 400;
}

.font-weight-semibold {
	font-weight: 500;
}

.font-weight-bold {
	font-weight: 600;
}

/* font-family */
.font-hind {
	font-family: 'Hind', sans-serif;
}

.font-kodachasan {
	font-family: 'Kodchasan', sans-serif;
}

.font-raleway {
	font-family: 'Raleway', sans-serif;
}

.font-pacifico {
	font-family: 'Pacifico', cursive;
}

/* font-size */
.font-size-16 {
	font-size: 16px;
}

.font-size-18 {
	font-size: 18px;
}

.font-size-20 {
	font-size: 20px;
}

.font-size-22 {
	font-size: 22px;
}

.font-size-24 {
	font-size: 24px;
}

.font-size-24 {
	font-size: 24px;
}

.font-size-26 {
	font-size: 26px;
}

.font-size-28 {
	font-size: 28px;
}

.font-size-30 {
	font-size: 30px;
}

.font-size-32 {
	font-size: 32px;
}

.font-size-34 {
	font-size: 34px;
}

.font-size-36 {
	font-size: 36px;
}

.font-size-38 {
	font-size: 38px;
}

.font-size-40 {
	font-size: 40px;
}

.font-size-42 {
	font-size: 42px;
}

.font-size-44 {
	font-size: 44px;
}

.font-size-46 {
	font-size: 46px;
}

.font-size-48 {
	font-size: 48px;
}

.font-size-50 {
	font-size: 50px;
}

/* text-align */
.text-center {
	text-align: center;
}

.text-left {
	text-align: left;
}

.text-right {
	text-align: right;
}

.text-justify {
	text-align: justify;
}

/* color */
.font-color-bn-petrol {
	color: #60a3b4;
}

.font-color-bn-hellpetrol {
	color: #709CD8;
}

.font-color-bn-rot {
	color: #bd1622;
}

.font-color-bn-grau {
	color: #626261;
}

.font-color-bn-header-grau {
	color: #F2F2F2;
}

.font-color-bn-lila {
	color: #776AB4;
}


/* background */
.background-white {
	background-color: white;
}

.background-opacity-2 {
	opacity: 0.2;
}

.border-radius-5 {
	border-radius: 5px;
}

.border-radius-10 {
	border-radius: 10px;
}

.border-radius-25 {
	border-radius: 25px;
}

.border-radius-50 {
	border-radius: 50px;
}


.height-25 {
	height: 50px;
}

.height-50 {
	height: 100px;
}


.owl-banner-box {
	height: 200px;
	max-height: 200px;
}

.height-100 {
	height: 66px;
}

.height-200 {
	height: 132px;
}

.height-300 {
	height: 198px;
}

</style>

[{/if}]