[{$smarty.block.parent}]

[{if $oView->getClassName() == "actions_main"}]

[{*
<link rel="stylesheet" href="[{$oViewConf->getModuleUrl('dre_categoryslider','out/src/js/jquery-3.3.1.min.js')}]">
*}]

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Das neueste kompilierte und minimierte JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<!-- Jquery UI -->
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

[{*
<script src="[{$oViewConf->getModuleUrl('dre_categoryslider','out/src/js/chosen.jquery.js')}]"></script>
<script src="[{$oViewConf->getModuleUrl('dre_categoryslider','out/src/js/chosen.proto.js')}]"></script>
*}]

[{*  select2 *}]

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

[{* Bootstrap Multiselect *}]
<script type="text/javascript" src="[{$oViewConf->getModuleUrl('dre_categoryslider','out/src/js/bootstrap-multiselect.js')}]"></script>

[{* color picker plugin *}]
<script type="text/javascript" src="[{$oViewConf->getModuleUrl('dre_categoryslider','out/src/js/spectrum.js')}]"></script>

<script>
    $(function () {



        $("#selectable").selectable({
            stop: function () {
                $("#feedback input").val('');
                var array = [];
                $(".ui-selected", this).each(function () {
                    var index = $("#selectable li").index(this);
                    array.push(index + 1);
                });
                $("#feedback input").val(array);


                /*
                var bannersemihead = $('#bannersemihead input').val();

                console.log(bannersemihead);

                var bannersemiheadcolor = $('#oxsemiheadcolor input').val();

                console.log(bannersemiheadcolor);

                var bannersemiheadclass = $('#bannersemiheadclass').val().join(' ');

                //console.log(bannersemiheadclass);

                var bannerhead = $('#bannerhead input').val();


                var bannerheadcolor = $('#bannerheadcolor input').val();
                //console.log(bannerheadcolor);

                var bannerheadclass = $('#bannerheadclass').val().join(' ');

                //var banneraddClass = [];

                var banneraddClass = $('#banneraddClass').val().join(' ');

                console.log(banneraddClass);

                var bannertext = $('#bannertext input').val();

                console.log(bannertext);

                var bannertextcolor = $('#bannertextcolor input').val();

                console.log(bannertextcolor);

                var bannertextclass = $('#bannertextclass').val().join(' ');
                */

                var numberarray = $("#feedback input").val().split(',').map(Number);

                var bannerEbenen = [];






                //console.log(bannerEbenen);
                //console.log(banneraddClass);
                /*$('#banner-debug-addclass input').val(banneraddClass);
                $('#htmltext input').val(bannerEbenen);
                $('#bannervorschau').html(bannerEbenen);*/
                //$('form').submit();

                var ebenen = baueBannerEbenen(numberarray);

                $('#bannervorschau').html('<div class="row"><div class="container-fluid">' + ebenen.ersteEbene + '</div></div><div class="row"><div class="container-fluid">' + ebenen.zweiteEbene + '</div></div><div class="row"><div class="container-fluid">'+ ebenen.dritteEbene +'</div></div>');
                $('#htmltext input').val('<div class="row"><div class="container-fluid">' + ebenen.ersteEbene + '</div></div><div class="row"><div class="container-fluid">' + ebenen.zweiteEbene + '</div></div><div class="row"><div class="container-fluid">' + ebenen.dritteEbene + '</div></div>');

                //console.log('<div class="row">' + ebenen.ersteEbene + '</div><div class="row">' + ebenen.zweiteEbene + '</div><div class="row">' + ebenen.dritteEbene + '</div>');

                $('#banner-ebenen-vorschau').html(JSON.stringify(ebenen , null , 2));
            }
        });
    });

function baueBannerEbenen(numberarray) {

    var retarray = [];
    var height = 0;
    var offset = 0;
    var col = 0;

    var e1 = 4;
    var e2 = 8;
    var e3 = 12;

    var banneraddClass ='';
    if (typeof $('#oxBannerAddClassInputId').val() !== 'undefined') {
        banneraddClass = $('#oxBannerAddClassInputId').val();
    }

    var bannerBoxColor = '';
    if (typeof $('#oxbannerboxcolorinputid').val() !== 'undefined') {
        bannerBoxColor = $('#oxbannerboxcolorinputid').val();
    }

    var bannersemihead = '';
    if(typeof $('#bannersemihead').val() !== 'undefined'){
        bannersemihead = $('#bannersemihead').val();
    }

    var bannersemiheadclass = '';
    if (typeof $('#oxBannerSemiHeadClassInputId').val() !== 'undefined') {
        bannersemiheadclass = $('#oxBannerSemiHeadClassInputId').val();
    }

    var bannersemiheadcolor = '';
    if (typeof $('#oxsemiheadcolorinputid').val() !== 'undefined') {
        bannersemiheadcolor = $('#oxsemiheadcolorinputid').val();
    }

    var bannerhead = '';
    if (typeof $('#bannerhead input').val() !== 'undefined') {
        bannerhead = $('#bannerhead input').val();
    }

    var bannerheadcolor = '';
    if (typeof $('#oxheadcolorinputid').val() !== 'undefined') {
        bannerheadcolor = $('#oxheadcolorinputid').val();
    }

    var bannerheadclass = '';
    if (typeof $('#oxbannerheadinputid').val() !== 'undefined') {
        bannerheadclass = $('#oxbannerheadinputid').val();
    }
    var bannertextclass = '';
    if (typeof $('#oxBannerTextClassInputId').val() !== 'undefined') {
        bannertextclass = $('#oxBannerTextClassInputId').val();
    }
    var bannertext = '';
    if (typeof $('#bannertext input').val() !== 'undefined') {
        bannertext = $('#bannertext input').val();
    }
    var bannertextcolor = '';
    if (typeof $('#oxbannertextcolorinputid').val() !== 'undefined') {
        bannertextcolor = $('#oxbannertextcolorinputid').val();
    }

    /*
    if( $('#bannersemihead input').val()){

    }
    */

    var obanner = {
        e1:[],
        e1boxcolor: '',
        e1height:0,
        e1col: 0,
        e1off: 0,
        e1semihead:'',
        e1semiheadcolor:'',
        e1semiheadclass:'',
        e1head:'',
        e1headcolor:'',
        e1headclass:'',
        e1addClass:'',
        e1textClass:'',
        e1text:'',
        e1textcolor:'',
        e2:[],
        e2boxcolor: '',
        e2height:0,
        e2col:3,
        e2off: 0,
        e2semihead:'',
        e2semiheadcolor: '',
        e2semiheadclass:'',
        e2head:'',
        e2headcolor: '',
        e2headclass:'',
        e2addClass:'',
        e2textClass: '',
        e2text: '',
        e2textcolor: '',
        e3:[],
        e3e1boxcolor: '',
        e3height:0,
        e3col:3,
        e3off:0,
        e3semihead:'',
        e3semiheadcolor: '',
        e3semiheadclass:'',
        e3head:'',
        e3headcolor: '',
        e3headclass:'',
        e3addClass:'',
        e3textClass: '',
        e3text: '',
        e3textcolor: '',
        test:[],
        ersteEbene:'',
        zweiteEbene:'',
        dritteEbene:''
    };


    for ( i = 0; i <= numberarray.length; i++ ){
        if( numberarray[i] <= 4 ){
            obanner.e1.push(numberarray[i]);
            //obanner.test.push(numberarray);
        }
        if( numberarray[i] > 4 && numberarray[i] <= 8 ){
            obanner.e2.push(numberarray[i]);
            //obanner.test.push(i);
        }
        if( numberarray[i] > 8 && numberarray[i] <= 12 ){
            obanner.e3.push(numberarray[i]);
            //obanner.test.push(i + 1);
        }
    }
    /* ebenen schleife */

    for (e = 0; e < 3; e++) {
        if(e === 0){
            if (typeof obanner.e1 !== 'undefined' && obanner.e1.length > 0) {
                obanner.e1boxcolor = bannerBoxColor;
                obanner.e1height = 100;
                obanner.e1semihead = bannersemihead;
                obanner.e1semiheadcolor = bannersemiheadcolor;
                obanner.e1semiheadclass = bannersemiheadclass;
                obanner.e1head = bannerhead;
                obanner.e1headcolor = bannerheadcolor;
                obanner.e1headclass = bannerheadclass;
                obanner.e1textClass = bannertextclass;
                obanner.e1text = bannertext;
                obanner.e1textcolor = bannertextcolor;
                obanner.e1addClass = banneraddClass;
                /* col berechnung */
                for( i = 0; i < obanner.e1.length;i++){
                    obanner.e1col = obanner.e1col + 3;
                }
                /* offset berechnung */
                for(i = 0; i < 4; i++){
                    if (obanner.e1[0] === 2){
                        obanner.e1off = 3;
                    }
                    if(obanner.e1[0] === 3) {
                        obanner.e1off = 6;
                    }
                    if(obanner.e1[0] === 4){
                        obanner.e1off = 9;
                    }
                }
                /* height berechnung */
                if (typeof obanner.e2 !== 'undefined' && obanner.e2.length > 0) {
                    obanner.e1height = 200;
                    if (typeof obanner.e3 !== 'undefined' && obanner.e3.length > 0) {
                        obanner.e1height = 300;
                    }
                }
            }else{
                obanner.e1boxcolor = '';
                obanner.e1height = 100;
                obanner.e1col = 3;
                obanner.e1addClass = '';
                obanner.e1headclass = '';
                obanner.e1headcolor = '';
                obanner.e1textClass = '';
                obanner.e1text = '';
                obanner.e1textcolor = '';
                obanner.e1semiheadclass = '';
                obanner.e1semiheadcolor ='';
            }
        }
        if (e === 1) {
            if (typeof obanner.e2 !== 'undefined' && obanner.e2.length > 0 && obanner.e1height === 100) {
                obanner.e2boxcolor = bannerBoxColor;
                obanner.e2height = 100;
                obanner.e2semihead = bannersemihead;
                obanner.e2semiheadcolor = bannersemiheadcolor;
                obanner.e2semiheadclass = bannersemiheadclass;
                obanner.e2head = bannerhead;
                obanner.e2headcolor = bannerheadcolor;
                obanner.e2headclass = bannerheadclass;
                obanner.e2textClass = bannertextclass;
                obanner.e2text = bannertext;
                obanner.e2textcolor = bannertextcolor;
                obanner.e2addClass = banneraddClass;
                /* col berechnung */
                for (i = 1; i < obanner.e2.length; i++) {
                    obanner.e2col = obanner.e2col + 3;
                }
                /* offset berechnung */
                for (i = 0; i < 4; i++) {
                    if (obanner.e2[0] === 6) {
                        obanner.e2off = 3;
                    }
                    if (obanner.e2[0] === 7) {
                        obanner.e2off = 6;
                    }
                    if (obanner.e2[0] === 8) {
                        obanner.e2off = 9;
                    }
                }
                if (typeof obanner.e3 !== 'undefined' && obanner.e3.length > 0) {
                    if(obanner.e1height === 100){
                        obanner.e1height = 100;
                    }
                    obanner.e2height = 200;
                } else {
                    obanner.e2height = 100;
                }
            } else {
                obanner.e2boxcolor = '';
                obanner.e2addClass = '';
                obanner.e2headclass = '';
                obanner.e2semiheadcolor = '';
                obanner.e2headcolor = '';
                obanner.e2semiheadclass = '';
                obanner.e2textClass = '';
                obanner.e2text = '';
                obanner.e2textcolor = '';
                obanner.e2height = 100;
            }
        }
        if(e === 2){
            if (typeof obanner.e3 !== 'undefined' && obanner.e3.length > 0 && obanner.e1height === 100 && obanner.e2height === 100) {
                obanner.e3boxcolor = bannerBoxColor;
                obanner.e1height = 100;
                obanner.e2height = 100;
                obanner.e3height = 100;
                obanner.e3semihead = bannersemihead;
                obanner.e3semiheadcolor = bannersemiheadcolor;
                obanner.e3semiheadclass = bannersemiheadclass;
                obanner.e3head = bannerhead;
                obanner.e3headcolor = bannerheadcolor;
                obanner.e3headclass = bannerheadclass;
                obanner.e3textClass = bannertextclass;
                obanner.e3text = bannertext;
                obanner.e3textcolor = bannertextcolor;
                obanner.e3addClass = banneraddClass;
                for (i = 1; i < obanner.e3.length; i++) {
                    obanner.e3col = obanner.e3col + 3;
                }
                /* offset berechnung */
                for (i = 0; i < 4; i++) {
                    if (obanner.e3[0] === 10) {
                        obanner.e3off = 3;
                    }
                    if (obanner.e3[0] === 11) {
                        obanner.e3off = 6;
                    }
                    if (obanner.e3[0] === 12) {
                        obanner.e3off = 9;
                    }
                }
            }else{
                obanner.e3boxcolor = '';
                obanner.e3addClass = '';
                obanner.e3headclass = '';
                obanner.e3semiheadcolor = '';
                obanner.e3headcolor = '';
                obanner.e3textClass = '';
                obanner.e3text = '';
                obanner.e3textcolor = bannertextcolor;
                obanner.e3semiheadclass = '';
            }
        }
    }

    obanner.ersteEbene  = '<div id="ersteEbene"  style="background-color:'+ obanner.e1boxcolor +';" class="col-xs-' + obanner.e1col + ' col-xs-offset-' + obanner.e1off + ' col-sm-' + obanner.e1col + ' ' + obanner.e1addClass + ' height-' + obanner.e1height + '"><div id="semihead1" style="color:'+ obanner.e1semiheadcolor +';" class="' + obanner.e1semiheadclass + '">' + obanner.e1semihead + '</div><div id="head1" style="color:'+ obanner.e1headcolor +';" class="' + obanner.e1headclass + '">' + obanner.e1head + '</div><div id="text1" style="color:'+ obanner.e1textcolor +';" class="'+ obanner.e1textClass +'">' + obanner.e1text + '</div> </div>'; //<div style="color: #' + bannersemiheadcolor + ';\" class=\"' + bannersemiheadclass + '\">' + bannersemihead + '</div><div style=\"color: #' + bannerheadcolor + ';\" class=\"' + bannerheadclass + '\">' + bannerhead + '</div><div style=\"color:#' + bannertextcolor + '\" class=\"' + bannertextclass + '\">' + bannertext + '</div></div>';
    obanner.zweiteEbene = '<div id="zweiteEbene" style="background-color:'+ obanner.e2boxcolor +';" class="col-xs-' + obanner.e2col + ' col-xs-offset-' + obanner.e2off + ' col-sm-' + obanner.e2col + ' ' + obanner.e2addClass + ' height-' + obanner.e2height + '"><div id="semihead2" style="color:'+ obanner.e2semiheadcolor +';" class="' + obanner.e2semiheadclass + '">' + obanner.e2semihead + '</div><div id="head2" style="color:'+ obanner.e2headcolor +';" class="' + obanner.e2headclass + '">' + obanner.e2head + '</div><div id="text2" style="color:'+ obanner.e2textcolor +';" class="'+ obanner.e2textClass +'">' + obanner.e2text + '</div> </div>';
    obanner.dritteEbene = '<div id="dritteEbene" style="background-color:'+ obanner.e3boxcolor +';" class="col-xs-' + obanner.e3col + ' col-xs-offset-' + obanner.e3off + ' col-sm-' + obanner.e3col + ' ' + obanner.e3addClass + ' height-' + obanner.e3height + '"><div id="semihead3" style="color:'+ obanner.e3semiheadcolor +';" class="' + obanner.e3semiheadclass + '">' + obanner.e3semihead + '</div><div id="head3" style="color:'+ obanner.e3headcolor +';" class="' + obanner.e3headclass + '">' + obanner.e3head + '</div><div id="text3" style="color:'+ obanner.e3textcolor +';" class="'+ obanner.e3textClass +'">' + obanner.e3text + '</div> </div>';

    /*if($.inArray(i, numberarray) !== -1){
		obanner.e1.push(i);
		//retarray.push( i );
	}*/

    return obanner;
    //retarray.join(' ');

    // bannerEbenen.push('<div id="ersteEbene" class="col-xs-12 col-sm-12 ' + banneraddClass + '"><div style="color: #' + bannersemiheadcolor + ';" class="' + bannersemiheadclass + '">' + bannersemihead + '</div><div style="color: #' + bannerheadcolor + ';" class="' + bannerheadclass + '">' + bannerhead + '</div><div style="color:#' + bannertextcolor + '" class="' + bannertextclass + '">' + bannertext + '</div></div>');

}
</script>


[{/if}]