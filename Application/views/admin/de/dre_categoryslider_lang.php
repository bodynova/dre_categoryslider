<?php

$sLangName  = "Deutsch";
$aLang = array(

	'charset' => 'UTF-8',

        // Actions Main Admin Template
        'DRE_PROMOTIONS_MAIN_TYPE_CATSLIDE'        => 'Kategorienslide',
        'DRE_PROMOTIONS_MAIN_ASSIGN_CATEGORY'      => 'Kategorie zuordnen',
        'DRE_PROMOTIONS_MAIN_ASSIGNEDDEFAULTCAT'   => 'Zugewiesene Kategorie:',
        'DRE_PROMOTIONS_BANNER_ASSIGNEDCATEGORY'   => 'Zugeordnete Kategorie',
        'DRE_SLIDER_CATEGORY'                      => 'Kategorie, in welcher das Slide angezeigt werden soll.',
        'DRE_INSTALL_SUCCESSFULL'                  => 'Modul Dre Slider für Kategorien erfolgreich installiert.',
        'GENERAL_AJAX_SORT_OXACTIVE'               => 'aktiv',
        'SEARCH_ACTIONS_TITLE'                     => 'Suche nach dem Aktionstitle...',
        'DRE_PROMOTIONS_BANNER_TYPE'               => 'Banner Typ auswählen',
        'DRE_PROMOTIONS_BANNER_TYPE_1'             => 'Banner Typ 1',
        'DRE_PROMOTIONS_BANNER_TYPE_2'             => 'Banner Typ 2',
        'DRE_PROMOTIONS_BANNER_TYPE_3'             => 'Banner Typ 3',
        'DRE_PROMOTIONS_BANNER_TYPE_4'             => 'Banner Typ 4',
        'DRE_PROMOTIONS_BANNER_HEAD'               => 'Banner Head',
        'DRE_PROMOTIONS_BANNER_HEAD_CLASS'         => 'Banner Head Klasse',
        'DRE_PROMOTIONS_BANNER_HEAD_COLOR'         => 'Banner Head Color',
        'DRE_PROMOTIONS_BANNER_SEMIHEAD'           => 'Banner Semi Head',
        'DRE_PROMOTIONS_BANNER_SEMIHEAD_COLOR'     => 'Banner Semi Head Color',
        'DRE_PROMOTIONS_BANNER_SEMIHEAD_CLASS'     => 'Banner Semi Head Klasse',
        'DRE_PROMOTIONS_BANNER_STYLE'              => 'Banner STYLE überschreibt auch KLASSEN!',
        'DRE_PROMOTIONS_BANNER_ADDCLASS'           => 'Banner Box Klasse hinzufügen',
        'DRE_PROMOTIONS_BANNER_TEXT'               => 'Banner Text',
        'DRE_PROMOTIONS_BANNER_TEXT_COLOR'         => 'Banner Text Color',
        'DRE_PROMOTIONS_BANNER_TEXT_CLASS'         => 'Banner Text Klasse hinzufügen',
        'DRE_PROMOTIONS_BANNER_POSITION'           => 'Banner Text-Boxen Position',
        'DRE_PROMOTIONS_BANNER_HTML'               => 'Banner KOMPLETT HTML',



);

