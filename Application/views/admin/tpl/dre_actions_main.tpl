[{include file="headitem.tpl" title="GENERAL_ADMIN_TITLE"|oxmultilangassign}]

[{if $readonly }]
    [{assign var="readonly" value="readonly disabled"}]
[{else}]
    [{assign var="readonly" value=""}]
[{/if}]

[{*}]
<script src="[{$oViewConf->getModuleUrl('dre_categoryslider','out/src/js/json.css.classes.json')}]"></script>
[{*}]

<script type="text/javascript">
<!--

function DeletePic( sField )
{
    var oForm = document.getElementById("myedit");
    document.getElementById(sField).value="";
    oForm.fnc.value='save';
    oForm.submit();
}

function baueoptions(dbvalue, idSelect, idInput)
{
    var bannerdbwerte = dbvalue;
    var Select = idSelect;
    var Input = idInput;
    $.getJSON("[{$oViewConf->getModuleUrl('dre_categoryslider','out/src/js/json.css.classes.json')}]", function (data) {
        var items = [];

        $.each(data, function () {
            var datakey = Object.keys(this)[0];
            var datavalue = this[datakey];
            var i = 0;
            $.each(datavalue, function () {
                var groupkey = Object.keys(datavalue)[i];
                i++;
                items.push('<optgroup label="' + groupkey + '">');
                var groupvalue = datavalue[groupkey];
                var j = 0;
                $.each(groupvalue, function () {
                    var optionkey = Object.keys(groupkey)[j]; //Object.keys(groupvalue)[j];
                    //console.log(groupvalue[optionkey]);
                    var optionvalue = groupvalue[optionkey];
                    j++;
                    var selected = '';
                    if ($.inArray(optionvalue.text, bannerdbwerte) !== -1) {
                        selected = 'selected';
                    }
                    items.push("<option id='" + optionvalue.id + "' " + selected + " >" + optionvalue.text + "</option>");
                });
                items.push('</optgroup>');
            });

        });
        $(Select).append(items.join(""));

        /* select2 */

        $(Select).multiselect();
        $(Select).on('change', function () {
            var wert = $(this).val().join(' ');
            $(Input).val(wert);
            //
            //
            //console.log(wert);
        });
    });

}

function baueFontOptions(dbvalue, idSelect, idInput){
    var bannerdbwerte = dbvalue;
    var Select = idSelect;
    var Input = idInput;
    $.getJSON("[{$oViewConf->getModuleUrl('dre_categoryslider','out/src/js/json.css.font-weight.json')}]", function (data) {
        var items = [];

        $.each(data, function () {
            var datakey = Object.keys(this)[0];
            var datavalue = this[datakey];
            var i = 0;
            $.each(datavalue, function () {
                var groupkey = Object.keys(datavalue)[i];
                i++;
                items.push('<optgroup label="' + groupkey + '">');
                var groupvalue = datavalue[groupkey];
                var j = 0;
                $.each(groupvalue, function () {
                    var optionkey = Object.keys(groupkey)[j]; //Object.keys(groupvalue)[j];
                    //console.log(groupvalue[optionkey]);
                    var optionvalue = groupvalue[optionkey];
                    j++;
                    var selected = '';
                    if ($.inArray(optionvalue.text, bannerdbwerte) !== -1) {
                        selected = 'selected';
                    }
                    items.push("<option id='" + optionvalue.id + "' " + selected + " >" + optionvalue.text + "</option>");
                });
                items.push('</optgroup>');
            });

        });
        $(Select).append(items.join(""));

        /* select2 */

        $(Select).multiselect();
        $(Select).on('change', function () {
            var wert = $(this).val().join(' ');
            $(Input).val(wert);
            //
            //
            //console.log(wert);
        });
    });
}




//console.log(bannersemiheadclass);
$(document).ready(function () {

    var oxBannerAddClassDb = "[{$edit->oxactions__oxbanneraddclass->value}]";
    var oxBannerAddClass = oxBannerAddClassDb.split(' ');
    var oxBannerAddClassSelectId = '#oxBannerAddClassSelectId';
    var oxBannerAddClassInputId = '#oxBannerAddClassInputId';
    baueoptions(oxBannerAddClass, oxBannerAddClassSelectId, oxBannerAddClassInputId);
    [{*}]$('#oxBannerAddClassSelectId').multiselect();[{*}]

    var oxbannersemiheadclassdb = "[{$edit->oxactions__oxbannersemiheadclass->value}]";
    var oxbannersemiheadclass = oxbannersemiheadclassdb.split(' ');
    var oxbannersemiheadclassselectid = '#oxBannerSemiHeadClassSelectId';
    var oxbannersemiheadclassinput = '#oxBannerSemiHeadClassInputId';
    baueoptions(oxbannersemiheadclass, oxbannersemiheadclassselectid, oxbannersemiheadclassinput);
    [{*}]$('#oxBannerSemiHeadClassSelectId').multiselect();[{*}]

    var oxbannerheadclassdb = "[{$edit->oxactions__oxbannerheadclass->value}]";
    var oxbannerheadclass = oxbannerheadclassdb.split(' ');
    var oxbannerheadselectid = '#oxbannerheadselectid';
    var oxbannerheadinputid = '#oxbannerheadinputid';
    baueoptions(oxbannerheadclass, oxbannerheadselectid, oxbannerheadinputid);
    [{*}]$('#oxbannerheadselectid').multiselect();[{*}]

    var oxBannerTextClassDb = "[{$edit->oxactions__oxbannertextclass->value}]";
    var oxBannerTextClass = oxBannerTextClassDb.split(' ');
    var oxBannerTextClassSelectId = '#oxBannerTextClassSelectId';
    var oxBannerTextClassInputId = '#oxBannerTextClassInputId';
    baueoptions(oxBannerTextClass, oxBannerTextClassSelectId, oxBannerTextClassInputId);
    [{*}]$('#oxBannerTextClassSelectId').multiselect();[{*}]

    $('#oxsemiheadcolorinputid').spectrum({
        preferredFormat: "hex",
        showPalette: true,
        color: '[{$edit->oxactions__oxsemiheadcolor->value}]',
        chooseText: "Genau diese Farbe",
        cancelText: "Keine davon",
        containerClassName: 'awesome',
        palette: [
            ['black', 'white', '#626261'],
            ['#60a3b4', '#709CD8', '#bd1622'],
            ['#F2F2F2']
        ]
    });

    $('#oxheadcolorinputid').spectrum({
        preferredFormat: "hex",
        showPalette: true,
        color: '[{$edit->oxactions__oxbannerheadcolor->value}]',
        chooseText: "Genau diese Farbe",
        cancelText: "Keine davon",
        containerClassName: 'awesome',
        palette: [
            ['black', 'white', '#626261'],
            ['#60a3b4', '#709CD8', '#bd1622'],
            ['#F2F2F2']
        ]
    });

    $('#oxbannertextcolorinputid').spectrum({
        preferredFormat: "hex",
        showPalette: true,
        color: '[{$edit->oxactions__oxbannertextcolor->value}]',
        chooseText: "Genau diese Farbe",
        cancelText: "Keine davon",
        containerClassName: 'awesome',
        palette: [
            ['black', 'white', '#626261'],
            ['#60a3b4', '#709CD8', '#bd1622'],
            ['#F2F2F2']
        ]
    });

    $('#oxbannerboxcolorinputid').spectrum({
        preferredFormat: "hex",
        showPalette: true,
        color: '[{$edit->oxactions__oxbannerboxcolor->value}]',
        chooseText: "Genau diese Farbe",
        cancelText: "Keine davon",
        containerClassName: 'awesome',
        palette: [
            ['black', 'white', '#626261'],
            ['#60a3b4', '#709CD8', '#bd1622'],
            ['#F2F2F2']
        ]
    });

    /*
    var oxBannerTextClassDb = "[{$edit->oxactions__oxbannertextclass->value}]";
    var oxBannerTextClass = oxBannerTextClassDb.split(' ');
    var oxBannerSemiHeadFontSize = '#oxBannerSemiHeadFontSize';
    var oxBannerTextClassInputId = '#oxBannerTextClassInputId';
    baueoptions(oxBannerTextClass, oxBannerSemiHeadFontSize, oxBannerTextClassInputId);
    */

    //oxBannerSemiHeadFontSize

});

//-->
</script>

<form name="transfer" id="transfer" action="[{$oViewConf->getSelfLink()}]" method="post">
    [{$oViewConf->getHiddenSid()}]
    <input type="hidden" name="oxid" value="[{$oxid}]">
    <input type="hidden" name="cl" value="actions_main">
</form>


<form name="myedit" enctype="multipart/form-data" id="myedit" onSubmit="copyLongDesc( 'oxactions__oxlongdesc' );" action="[{$oViewConf->getSelfLink()}]" method="post">
[{$oViewConf->getHiddenSid()}]
<input type="hidden" name="cl" value="actions_main">
<input type="hidden" name="fnc" value="">
<input type="hidden" name="oxid" value="[{$oxid}]">
<input type="hidden" name="editval[oxactions__oxid]" value="[{$oxid}]">
<input type="hidden" name="sorting" value="">
<input type="hidden" name="stable" value="">
<input type="hidden" name="starget" value="">
<input type="hidden" name="editval[oxactions__oxlongdesc]" value="">

[{if ($edit->oxactions__oxtype->value == 3 && $oViewConf->isAltImageServerConfigured()) || ($edit->oxactions__oxtype->value == 9 && $oViewConf->isAltImageServerConfigured())}]
     <div class="warning">[{oxmultilang ident="ALTERNATIVE_IMAGE_SERVER_NOTE"}] [{oxinputhelp ident="HELP_ALTERNATIVE_IMAGE_SERVER_NOTE"}]</div>
[{/if}]

<table cellspacing="0" cellpadding="0" border="0" width="98%">

<tr>
    <td valign="top" class="edittext" style="padding-right: 20px;">
        <table cellspacing="0" cellpadding="0" border="0">
        [{block name="admin_actions_main_form"}]
            <tr>
                <td class="edittext" width="120">
                    [{oxmultilang ident="GENERAL_NAME"}]
                </td>
                <td class="edittext">
                    <input type="text" class="editinput" size="32" maxlength="[{$edit->oxactions__oxtitle->fldmax_length}]" name="editval[oxactions__oxtitle]" value="[{$edit->oxactions__oxtitle->value}]" [{$readonly}]>
                    [{oxinputhelp ident="HELP_GENERAL_NAME"}]
                </td>
            </tr>

            <tr>
              <td class="edittext" width="120">
                [{if $edit->oxactions__oxtype->value != 2}]
                  [{oxmultilang ident="GENERAL_ALWAYS_ACTIVE"}]
                [{else}]
                  [{oxmultilang ident="GENERAL_ACTIVE"}]
                [{/if}]
              </td>
              <td class="edittext">
                <input class="edittext" type="checkbox" name="editval[oxactions__oxactive]" value='1' [{if $edit->oxactions__oxactive->value == 1}]checked[{/if}] [{$readonly}]>
                [{oxinputhelp ident="HELP_GENERAL_ACTIVE"}]
              </td>
            </tr>
            <tr>
              <td class="edittext">
                  [{if $edit->oxactions__oxtype->value != 2 }][{oxmultilang ident="GENERAL_ACTIVFROMTILL"}][{/if}]&nbsp;
              </td>
              <td class="edittext" align="right">
                [{oxmultilang ident="GENERAL_FROM"}] <input type="text" class="editinput" size="27" name="editval[oxactions__oxactivefrom]" value="[{$edit->oxactions__oxactivefrom|oxformdate}]" [{include file="help.tpl" helpid=article_vonbis}] [{$readonly}]><br>
                [{oxmultilang ident="GENERAL_TILL"}] <input type="text" class="editinput" size="27" name="editval[oxactions__oxactiveto]" value="[{$edit->oxactions__oxactiveto|oxformdate}]" [{include file="help.tpl" helpid=article_vonbis}] [{$readonly}]>
                [{if $edit->oxactions__oxtype->value != 2}][{oxinputhelp ident="HELP_GENERAL_ACTIVFROMTILL"}][{/if}]
              </td>
            </tr>
            [{if $oxid == "-1"}]
                <tr>
                    <td class="edittext">
                        [{oxmultilang ident="GENERAL_TYPE"}]&nbsp;
                    </td>
                  <td class="edittext">
                    <select class="editinput" name="editval[oxactions__oxtype]">
                      <option value="1">[{oxmultilang ident="PROMOTIONS_MAIN_TYPE_ACTION"}]</option>
                      <option value="2">[{oxmultilang ident="PROMOTIONS_MAIN_TYPE_PROMO"}]</option>
                      <option value="3">[{oxmultilang ident="PROMOTIONS_MAIN_TYPE_BANNER"}]</option>
                      <option value="9">[{oxmultilang ident="DRE_PROMOTIONS_MAIN_TYPE_CATSLIDE"}]</option>
                    </select>
                  </td>
                </tr>
            [{/if}]
            [{if $edit->oxactions__oxid->value!="" && ($edit->oxactions__oxtype->value == 0 || $edit->oxactions__oxtype->value == 1)}]
            </tbody>
        </table>
    </td>
    <td class="edittext" valign="top" style="width:65%;padding-right: 20px;">
        <table cellspacing="5" cellpadding="0" border="0" style="position:absolute; ">
            <tbody>
            <tr>
                <td></td>
                <td>
                    <a href="[{$oViewConf->getBaseDir()}]index.php?cl=dre_landingpage&actionlist=[{$edit->oxactions__oxid->value}]"
                       target="_blank">
                        Landingpage: index.php?cl=dre_landingpage&actionlist=[{$edit->oxactions__oxid->value}]
                    </a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>[{$editor}]</td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>
    <tr>
        <td class="edittext" valign="top" style="padding-right: 20px;">
            <table cellspacing="0" cellpadding="0" border="0">
                <tbody>
                [{/if}]
        [{/block}]
        <tr>
            <td class="edittext">
            </td>
            <td class="edittext"><br>
                [{include file="language_edit.tpl"}]
            </td>
        </tr>
        [{if $edit->oxactions__oxtype->value == 3 || $edit->oxactions__oxtype->value == 9}]
            <td class="edittext" width="120">
                [{oxmultilang ident="GENERAL_SORT"}]
            </td>
            <td class="edittext">
                <input type="text" class="editinput" size="32" maxlength="[{$edit->oxactions__oxsort->fldmax_length}]" name="editval[oxactions__oxsort]" value="[{$edit->oxactions__oxsort->value}]" [{$readonly}]>
                [{oxinputhelp ident="HELP_GENERAL_SORT"}]
            </td>
        [{/if}]
            <tr>
                <td class="edittext">
                </td>
                <td class="edittext"><br>
                    <input type="submit" class="edittext" name="save" value="[{ oxmultilang ident="GENERAL_SAVE"}]" onClick="Javascript:document.myedit.fnc.value='save'" [{$readonly}] ><br><br>

                    [{if $oxid != "-1"}]
                        [{if $edit->oxactions__oxtype->value < 2 }]
                           <input type="button" value="[{oxmultilang ident="GENERAL_ASSIGNARTICLES"}]" class="edittext" onclick="JavaScript:showDialog('&cl=actions_main&aoc=1&oxid=[{$oxid}]');" [{$readonly}]>
                        [{else}]
                            <input type="button" value="[{oxmultilang ident="GENERAL_ASSIGNGROUPS"}]" class="edittext" onclick="JavaScript:showDialog('&cl=actions_main&oxpromotionaoc=groups&oxid=[{$oxid}]');" [{$readonly}]>
                        [{/if}]
                    [{/if}]
                </td>
            </tr>
        </table>
    </td>
    [{if $edit->oxactions__oxtype->value > 1 }]

        [{if $edit->oxactions__oxtype->value == 3 || $edit->oxactions__oxtype->value == 9 }]
            <td width="180" valign="top" style="padding: 0 25px 0 25px; border-left: 1px solid #ddd;">
            [{if (!($edit->oxactions__oxpic->value=="nopic.jpg" || $edit->oxactions__oxpic->value=="")) }]
                <div style="padding-bottom: 10px;">
                    <a href="[{$edit->getBannerPictureUrl()}]" target="_blank">
                        <img src="[{$edit->getBannerPictureUrl()}]" width="120px;" border="0">
                    </a>
                    <div style="width: 120px; color: #666; padding-top: 5px; border-top: 1px solid #ccc; text-align: center;">
                        Banner picture
                    </div>
                </div>
            [{/if}]
            </td>
        [{/if}]

        <td valign="top" class="edittext" align="left" style="width:100%;padding-left:5px;padding-bottom:10px;">
            <table cellspacing="0" cellpadding="0" border="0">
                [{if $edit->oxactions__oxtype->value == 2 }]
                    [{block name="admin_actions_main_editor"}]
                        <!-- Promotions editor -->
                        <tr>
                            <td class="edittext" width="100%" colspan="2">
                                [{$editor}]
                            </td>
                        </tr>
                    [{/block}]
                [{/if}]

                [{if $edit->oxactions__oxtype->value == 3 || $edit->oxactions__oxtype->value == 9}]
                <!-- Banners picture upload and link -->
                <tr>
                    <td class="edittext">
                        <table cellspacing="0" cellpadding="0" width="100%" border="0" class="listTable">
                          [{block name="admin_actions_main_product"}]
                              <colgroup>
                                  <col width="1%" nowrap>
                                  <col width="1%" nowrap>
                                  <col width="98%">
                              </colgroup>
                              <tr>
                                  <th colspan="5" valign="top">
                                     [{oxmultilang ident="PROMOTIONS_BANNER_PICTUREANDLINK"}]
                                     [{oxinputhelp ident="HELP_PROMOTIONS_BANNER_PICTUREANDLINK"}]
                                  </th>
                              </tr>

                              <tr>
                                <td class="text">
                                    <b>[{oxmultilang ident="PROMOTIONS_BANNER_PICTUREUPLOAD"}] ([{oxmultilang ident="GENERAL_MAX_FILE_UPLOAD"}] [{$sMaxFormattedFileSize}], [{oxmultilang ident="GENERAL_MAX_PICTURE_DIMENSIONS"}]):</b>
                                </td>
                                <td class="edittext">
                                    <input class="editinput" name="myfile[PROMO@oxactions__oxpic]" type="file" size="26"[{$readonly_fields}]>
                                    <input id="oxpic" type="hidden" maxlength="[{$edit->oxactions__oxpic->fldmax_length}]" name="editval[oxactions__oxpic]" value="[{$edit->oxactions__oxpic->value}]" readonly>
                                </td>
                                <td nowrap="nowrap">
                                    [{if (!($edit->oxactions__oxpic->value=="nopic.jpg" || $edit->oxactions__oxpic->value=="")) && !$readonly }]
                                        <div style="display: inline-block;">
                                            <a href="Javascript:DeletePic('oxpic');" class="deleteText"><span class="ico"></span><span style="float: left;">[{oxmultilang ident="GENERAL_DELETE"}]</span></a>
                                        </div>
                                    [{/if}]
                                </td>
                              </tr>

                              [{assign var="_oArticle" value=$edit->getBannerArticle()}]

                              <tr>
                                <td class="text">
                                    <b>[{oxmultilang ident="PROMOTIONS_BANNER_LINK"}]:</b>
                                </td>
                                <td class="text">
                                    <input type="text" class="editinput" size="43" name="editval[oxactions__oxlink]" value="[{$edit->oxactions__oxlink->value}]" [{$readonly}]>
                                </td>
                                <td nowrap="nowrap">
                                    [{if $edit->oxactions__oxlink->value }]
                                        <div style="display: inline-block;">
                                            <a href="[{$edit->getBannerLink()}]" class="zoomText" target="_blank"><span class="ico"></span><span style="float: left;">[{oxmultilang ident="ARTICLE_PICTURES_PREVIEW"}]</span></a>
                                        </div>
                                    [{/if}]
                                </td>
                              </tr>

                              <tr>
                                <td class="text">
                                    <b>[{oxmultilang ident="PROMOTIONS_BANNER_ASSIGNEDARTICLE"}]:</b>
                                </td>
                                <td class="text" colspan="2">
                                    <b>
                                        <span id="assignedArticleTitle">
                                        [{if $_oArticle}]
                                            [{$_oArticle->oxarticles__oxartnum->value}] [{$_oArticle->oxarticles__oxtitle->value}]
                                        [{else}]
                                            ---
                                        [{/if}]
                                        </span>
                                    </b>
                                </td>
                              </tr>

                            [{if $edit->oxactions__oxtype->value == 9 }]
                              [{assign var="_oCategory" value=$edit->getSlideCategory()}]
                              <tr>
                                <td class="text">
                                    <b>[{oxmultilang ident="DRE_PROMOTIONS_BANNER_ASSIGNEDCATEGORY"}]:</b>
                                </td>
                                <td class="text" colspan="2">
                                    <b>
                                        <span id="assignedCategoryTitle">
                                        [{if $_oCategory}]
                                            [{$_oCategory->oxcategories__oxtitle->value}] - [{$_oCategory->oxcategories__oxdesc->value}]
                                        [{else}]
                                            ---
                                        [{/if}]
                                        </span>
                                    </b>
                                </td>
                              </tr>
                            [{/if}]

                            [{* Banner Bild zuordnungen *}]

                            [{if $edit->oxactions__oxtype->value == 3}]

                                [{* Bannertyp
                                <tr>
                                    <td class="text">
                                        <b>
                                            [{oxmultilang ident="DRE_PROMOTIONS_BANNER_TYPE"}]
                                        </b>
                                    </td>
                                    <td class="edittext" colspan="3">
                                        <select class="editinput" name="editval[oxactions__oxbannertyp]"> onchange="alert(this.value)">
                                            <option value="1">[{oxmultilang ident="DRE_PROMOTIONS_BANNER_TYPE_1"}]</option>
                                            <option value="2">[{oxmultilang ident="DRE_PROMOTIONS_BANNER_TYPE_2"}]</option>
                                            <option value="3">[{oxmultilang ident="DRE_PROMOTIONS_BANNER_TYPE_3"}]</option>
                                            <option value="4">[{oxmultilang ident="DRE_PROMOTIONS_BANNER_TYPE_4"}]</option>
                                        </select>
                                    </td>
                                </tr>

                                *}]
	                            [{* BannerBox *}]
	                            <tr>
		                            <td class="text">
			                            <b>
				                            [{oxmultilang ident="DRE_PROMOTIONS_BANNER_ADDCLASS"}]
			                            </b>
		                            </td>
		                            <td class="text">
			                            <select id="oxBannerAddClassSelectId" class="js-example-basic-multiple form-control" multiple="multiple" data-placeholder="Klassen f&uuml;r die Banner Box"></select>
                                        <input id="oxbannerboxcolorinputid" name="editval[oxactions__oxbannerboxcolor]" value="[{$edit->oxactions__oxbannerboxcolor->value}]"> [{$edit->oxactions__oxbannerboxcolor->value}]
			                            <input id="oxBannerAddClassInputId" type="text" class="editinput" size="43" name="editval[oxactions__oxbanneraddclass]" value="[{$edit->oxactions__oxbanneraddclass->value}]" [{$readonly}]>
		                            </td>
	                            </tr>

                                [{* Banner Semi Head *}]
                                <tr>
                                    <td class="text">
                                        <b>
                                            [{oxmultilang ident="DRE_PROMOTIONS_BANNER_SEMIHEAD"}]
                                        </b>
                                        <input id="oxBannerSemiHeadClassInputId" type="text" class="editinput" size="25" name="editval[oxactions__oxbannerposition]" value="[{$edit->oxactions__oxbannersemiheadclass->value}]" [{$readonly}]>
                                    </td>
                                    <td class="text">
                                        <input id="bannersemihead" type="text" class="editinput" size="43" name="editval[oxactions__oxbannersemihead]" value="[{$edit->oxactions__oxbannersemihead->value}]" [{$readonly}]>
                                        <input id="oxsemiheadcolorinputid" name="editval[oxactions__oxsemiheadcolor]" value="[{$edit->oxactions__oxsemiheadcolor->value}]"> [{$edit->oxactions__oxsemiheadcolor->value}]
                                        <select id="oxBannerSemiHeadClassSelectId" class="js-example-basic-multiple form-control" multiple="multiple" data-placeholder="Klassen f&uuml;r den Semi Head"></select>
                                        <select id="oxBannerSemiHeadFontSize" class="js-example-basic-multiple form-control" multiple="multiple" data-placeholder="Klassen f&uuml;r den Semi Head"></select>
                                        [{*}]<input id="oxBannerSemiHeadClassInputId" type="text" class="editinput" size="43" name="editval[oxactions__oxbannersemiheadclass]" value="[{$edit->oxactions__oxbannersemiheadclass->value}]" [{$readonly}]>[{*}]
                                    </td>
                                </tr>

                                [{* Banner Head *}]
                                <tr>
                                    <td class="text">
                                        <b>
                                            [{oxmultilang ident="DRE_PROMOTIONS_BANNER_HEAD"}]
                                        </b>
                                    </td>
                                    <td id="bannerhead" class="text">
                                        <input type="text" class="editinput" size="43" name="editval[oxactions__oxbannerhead]" value="[{$edit->oxactions__oxbannerhead->value}]" [{$readonly}]>
                                        <input id="oxheadcolorinputid" type="text" name="editval[oxactions__oxbannerheadcolor]" value="[{$edit->oxactions__oxbannerheadcolor->value}]" [{$readonly}]>[{$edit->oxactions__oxbannerheadcolor->value}]
                                        <select id="oxbannerheadselectid" class="js-example-basic-multiple form-control" multiple="multiple" data-placeholder="Klassen f&uuml;r den Head"></select>
                                        <input id="oxbannerheadinputid" type="text" class="editinput" size="43" name="editval[oxactions__oxbannerheadclass]" value="[{$edit->oxactions__oxbannerheadclass->value}]" [{$readonly}]>
                                    </td>
                                </tr>
	                            [{* Banner Text *}]
	                            <tr>
		                            <td class="text">
			                            <b>
				                            [{oxmultilang ident="DRE_PROMOTIONS_BANNER_TEXT"}]
			                            </b>
		                            </td>
		                            <td id="bannertext" class="text">
			                            <input type="text" class="editinput" size="43" name="editval[oxactions__oxbannertext]" value="[{$edit->oxactions__oxbannertext->value}]" [{$readonly}]>
			                            <input id="oxbannertextcolorinputid" type="text" name="editval[oxactions__oxbannertextcolor]" value="[{$edit->oxactions__oxbannertextcolor->value}]" [{$readonly}]> [{$edit->oxactions__oxbannertextcolor->value}]
			                            <select id="oxBannerTextClassSelectId" class="js-example-basic-multiple form-control" multiple="multiple" data-placeholder="Klassen f&uuml;r den Bannertext"></select>
			                            <input id="oxBannerTextClassInputId" type="text" class="editinput" size="43" name="editval[oxactions__oxbannertextclass]" value="[{$edit->oxactions__oxbannertextclass->value}]" [{$readonly}]>
		                            </td>
	                            </tr>


                                [{* Bannerstyle
                                <tr>
                                    <td class="text">
                                        <b>
                                            [{oxmultilang ident="DRE_PROMOTIONS_BANNER_STYLE"}]
                                        </b>
                                    </td>
                                    <td class="text">
                                        <input type="text" class="editinput" size="43" name="editval[oxactions__oxbannerstyle]" value="[{$edit->oxactions__oxbannerstyle->value}]" [{$readonly}]>
                                    </td>
                                </tr>
                                *}]

	                            [{* Banner Box Position *}]
                                <tr>
                                    <td class="text">
                                        <b>
                                            [{oxmultilang ident="DRE_PROMOTIONS_BANNER_POSITION"}]
                                        </b>
                                        <p id="feedback">
                                            <input id="select-result" type="text" class="editinput" size="25" name="editval[oxactions__oxbannerposition]" value="[{$edit->oxactions__oxbannerposition->value}]" [{$readonly}]>
                                        </p>
                                    </td>
                                    <td>
                                        <ol id="selectable">
                                            <li class="ui-state-default">1</li>
                                            <li class="ui-state-default">2</li>
                                            <li class="ui-state-default">3</li>
                                            <li class="ui-state-default">4</li>
                                            <li class="ui-state-default">5</li>
                                            <li class="ui-state-default">6</li>
                                            <li class="ui-state-default">7</li>
                                            <li class="ui-state-default">8</li>
                                            <li class="ui-state-default">9</li>
                                            <li class="ui-state-default">10</li>
                                            <li class="ui-state-default">11</li>
                                            <li class="ui-state-default">12</li>
                                        </ol>
                                    </td>
                                </tr>

	                            [{* DEBUG BANNER EBENEN
                                <tr>
                                    <td>
                                        BannerEbenen
                                    </td>
                                    <td>
                                        <pre><div id="banner-ebenen-vorschau"></div></pre>
                                    </td>
                                </tr>
                                *}]


                                [{* Vorschaubild *}]
                                <tr>
                                    [{* Banner Vorschaubild mit angewendeten Klassen *}]
                                    [{*if $edit->oxactions__oxbannertyp->value *}]
                                        <td colspan="3" height="300" width="960">
                                            <div class="owl-banner-box" style="background-image: url('[{$edit->getBannerPictureUrl()}]'); background-repeat:no-repeat;background-position:center;background-size: cover; width: 960px;height:100%;">

                                                    <div id="bannervorschau">
                                                        [{$edit->oxactions__oxhtml->value|html_entity_decode}]
                                                    </div>

                                            </div>
                                        </td>
                                    [{*/if*}]
                                </tr>

                                [{* html text *}]
                                <tr>
                                    <td>
                                        [{oxmultilang ident="DRE_PROMOTIONS_BANNER_HTML"}]
                                    </td>
                                    <td id="htmltext">
                                        <input class="editinput" type="text" size="43" name="editval[oxactions__oxhtml]" value="[{$edit->oxactions__oxhtml->value}]">
                                    </td>
                                </tr>
                                [{* banner add Class
                                <tr>
                                    <td>
                                        <b>Banner Add Class</b>
                                    </td>
                                    <td id="banner-debug-addclass">
                                        <input class="editinput" type="text" size="43" name="editval[oxactions__oxbanneraddclass]" value="[{$edit->oxactions__oxbanneraddclass->value}]">
                                    </td>
                                </tr>
                                *}]

                            [{/if}]

                            
                          [{/block}]
                        </table>

                        <input type="button" value="[{oxmultilang ident="GENERAL_ASSIGNARTICLE"}]" class="edittext" onclick="JavaScript:showDialog('&cl=actions_main&oxpromotionaoc=article&oxid=[{$oxid}]');" [{$readonly}]>
                        [{if $edit->oxactions__oxtype->value == 9 }]
                            <input type="button" value="[{oxmultilang ident="DRE_PROMOTIONS_MAIN_ASSIGN_CATEGORY"}]" class="edittext" onclick="JavaScript:showDialog('&cl=actions_main&oxpromotionaoc=category&oxid=[{$oxid}]');" [{$readonly}]>
                        [{/if}]
                    </td>


                </tr>
                [{/if}]
            </table>
        </td>
        <!-- Ende rechte Seite -->
    [{/if}]
    </tr>
</table>
</form>




</div>

<!-- START new promotion button -->
<div class="actions">
    [{strip}]
        <ul>
            <li><a [{if !$firstitem}]class="firstitem"[{assign var="firstitem" value="1"}][{/if}] id="btn.new" href="#" onClick="Javascript:top.oxid.admin.editThis( -1 );return false;" target="edit">[{oxmultilang ident="TOOLTIPS_NEWPROMOTION"}]</a> |</li>
                [{include file="bottomnavicustom.tpl"}]

                [{if $sHelpURL }]
                    [{* HELP *}]
                    <li><a [{if !$firstitem}]class="firstitem"[{assign var="firstitem" value="1"}][{/if}] id="btn.help" href="[{$sHelpURL}]/[{$oViewConf->getActiveClassName()|oxlower}].html" OnClick="window.open('[{$sHelpURL}]/[{$oViewConf->getActiveClassName()|lower}].html','OXID_Help','width=800,height=600,resizable=no,scrollbars=yes');return false;">[{oxmultilang ident="TOOLTIPS_OPENHELP"}]</a></li>
                [{/if}]
        </ul>
    [{/strip}]
</div>

<!-- END new promotion button -->

[{include file="bottomitem.tpl"}]
