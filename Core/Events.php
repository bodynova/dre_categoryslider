<?php

namespace Bender\dre_CategorySlider\Core;

use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;
use OxidEsales\Eshop\Core\ConfigFile;
use OxidEsales\Eshop\Core\FileCache;
use OxidEsales\Eshop\Core\Registry;



class Events
{
    /*
     * Path to SQL
     */
    protected static $_sSqlPath = 'modules/bender/dre_CategorySlider/install/install.sql';
    
    /**
     * Executes SQL file install
     *
     */
    public static function onActivate(){

        // Datenbank-Objekt auslesen
        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);

        $aSql = [];
        $aSql[] = <<<SQL
CREATE TABLE IF NOT EXISTS `dreaction2category` (
  `OXID` varchar(32) NOT NULL COMMENT 'Record id',
  `OXACTIONID` varchar(32) NOT NULL COMMENT 'Action id (oxactions)',
  `OXCATID` varchar(32) NOT NULL COMMENT 'Category id (oxcategories)',
  PRIMARY KEY (`OXID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL;
        foreach ($aSql as $sSql) {
            // MySQL-Fehler abfangen
            try {
                $blResult = $oDb->execute($sSql);
            } catch (DatabaseErrorException $e) {
                // Ausser es sind keine "alread exists" fehler...
                if (!preg_match('/(already exists|Duplicate column name)/i', $e->getMessage())) {
                    throw $e;
                }
            }
        }
        self::clearCache();
        return $blResult;
    }

    public static function onDeactivate(){
        self::clearCache();
    }

    /**
     * Löscht den TMP-Ordner sowie den Smarty-Ordner
     */
    protected static function clearCache()
    {
        /** @var FileCache $fileCache */
        $fileCache = oxNew( FileCache::class );
        $fileCache::clearCache();

        /** Smarty leeren */
        $tempDirectory = Registry::get( ConfigFile::class )->getVar("sCompileDir");
        $mask = $tempDirectory . "/smarty/*.php";
        $files = glob($mask);
        if (is_array($files)) {
            foreach ($files as $file) {
                if (is_file($file)) {
                    @unlink($file);
                }
            }
        }
    }
}
