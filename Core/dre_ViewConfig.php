<?php
namespace Bender\dre_CategorySlider\Core;

class dre_ViewConfig extends dre_ViewConfig_parent {
    
    
    /**
     * Returns true if slides exist for active category
     * 
     * @param String $sCategoryId 
     *
     * @return Boolean
     */
    public function hasCategorySlides($sCategoryId) 
    {
        $bReturn = false;
        if ($sCategoryId && $sCategoryId != "-1") {  
            $oSlideList = oxNew('oxActionList');
            $bReturn = $oSlideList->areAnyActiveSlides($sCategoryId);
        }      
        return $bReturn;
    }
    
    /**
     * Returns List of slides for active category
     * 
     * @param String $sCategoryId 
     *
     * @return oxactionlist $oSlideList
     */
    public function getSlidesOfCategory($sCategoryId)
    {
        $oSlideList = null;

        if ($sCategoryId && $sCategoryId != "-1") {        
            $oSlideList = oxNew('oxActionList');
            $oSlideList->loadSlides($sCategoryId);
        }
        return $oSlideList;
    }
    
    
    
    
}
