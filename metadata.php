<?php
/**
 * Metadata version
 */
$sMetadataVersion = '2.0';

/**
 * Module information
 */
$aModule = array(
    'id'            => 'dre_categoryslider',
    'title'         => '<img src="../modules/bender/dre_categoryslider/out/img/favicon.ico" title="Bodynova Kategorie Slider Modul">odynova Slider für Kategorien',
    'description' => [
        'de' => 'Slider in den Kategorien',
        'en' => 'slider also in Categorys'
    ],
    'version'     => '2.0.0',
    'author'      => 'André Bender',
    'thumbnail'   => 'out/img/logo_bodynova.png',
    'url'         => 'https://bodynova.de',
    'email'       => 'support@bodynova.de',
    'controllers' => array(
        'actionscategory_ajax'  => \Bender\dre_CategorySlider\Application\Controller\Admin\ActionsCategoryAjax::class
    ),
    'extend' => array(
        \OxidEsales\Eshop\Core\ViewConfig::class                                    => \Bender\dre_CategorySlider\Core\dre_ViewConfig::class,
        \OxidEsales\Eshop\Application\Controller\Admin\ActionsMain::class           => \Bender\dre_CategorySlider\Application\Controller\Admin\dre_actionsmain::class,
        \OxidEsales\Eshop\Application\Model\Actions::class                          => \Bender\dre_CategorySlider\Application\Model\dre_Actions::class,
        \OxidEsales\Eshop\Application\Model\ActionList::class                       => \Bender\dre_CategorySlider\Application\Model\dre_ActionList::class,

    ),
    'blocks'        => array(
        array(
            'template'                  => 'page/list/list.tpl',
            'block'                     => 'page_list_listhead',
            'file'                      => 'Application/views/blocks/page/list/dre_category_slider_list.tpl'
        ),
        array(
            'template'                  => 'actions_list.tpl',
            'block'                     => 'admin_actions_list_item',
            'file'                      => 'Application/views/admin/blocks/dre_actions_list.tpl'
        ),
        array(
            'template'                  => 'headitem.tpl',
            'block'                     => 'admin_headitem_inccss',
            'file'                      => 'Application/views/admin/blocks/dre_admin_headitem_inccss.tpl'
        ),
        array(
            'template'                  => 'headitem.tpl',
            'block'                     => 'admin_headitem_incjs',
            'file'                      => 'Application/views/admin/blocks/dre_admin_headitem_incjs.tpl'
        ),
	),
    'templates' => array(
                        // admin
                        'dre_actions_main.tpl'             => 'bender/dre_categoryslider/Application/views/admin/tpl/dre_actions_main.tpl',
                        //
                        'actionscategory.tpl'              => 'bender/dre_categoryslider/Application/views/admin/tpl/popups/actionscategory.tpl',
                        // widget
                        'dre_category_slider.tpl'          => 'bender/dre_categoryslider/Application/views/tpl/widget/dre_category_slider.tpl',
    ),  
    'events'    => array(
                        'onActivate'                        => 'Bender\dre_CategorySlider\Core\Events::onActivate',
                        'onDeactivate'                      => 'Bender\dre_CategorySlider\Core\Events::onDeactivate',
    ),
);
