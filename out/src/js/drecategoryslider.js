/*
 * Purpose:     jQuery for drecategoryslider module.
 *              initializes the slider
 *              Part of Module drecategoryslider
 */
$( window ).load( function(){
    $( '#dre-category-slider' ).flexslider(
        {
            animation: "slide"
        }
    );
} );

